﻿using System.Collections.Generic;

namespace Bitz.Modules.Core.Input
{
    public interface IInputListener
    {
        void ProcessInputEvents(List<InputEvent> activeInputEvents);
    }
}