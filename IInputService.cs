﻿using System;
using Bitz.Modules.Core.Foundation.Services;

namespace Bitz.Modules.Core.Input
{
    public interface IInputService : IService
    {
        void AddInputListener(IInputListener inputListener, Int32 priority);
        void RemoveInputListener(IInputListener inputListener);
        void AddPressable(IPressable pressable);
        void RemovePressable(IPressable pressable);
    }
}