﻿using System;
using OpenTK;

namespace Bitz.Modules.Core.Input
{
    public interface IPressable
    {
        Action<InputEvent> OnPress { get; set; }
        Action<InputEvent> OnRelease { get; set; }
        Boolean TestPressCoordinates(Vector2 pressCoordinates);
        Int32 PressablePriortiy { get; }
        TimeSpan NextCanPress { get; set; }
    }
}
