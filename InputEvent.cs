﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation.Logic;
using OpenTK;

namespace Bitz.Modules.Core.Input
{
    public class InputEvent : BasicObject
    {
        /// <summary>
        /// The event types that are produced by the engine
        /// </summary>
        public enum InputType
        {
            /// <summary>
            /// Mouse left click or touch event
            /// </summary>
            LEFT_CLICK,
            /// <summary>
            /// Mouse Right Click
            /// </summary>
            RIGHT_CLICK,
            /// <summary>
            /// Mouse Middle Click
            /// </summary>
            MIDDLE_CLICK,
            /// <summary>
            /// Keyboard event
            /// </summary>
            KEY
        }

        private List<Action<InputEvent>> _OnCompleteActions;

        /// <summary>
        /// The current position associated with the event
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        /// The type of the event
        /// </summary>
        public InputType TypeOfInput { get; set; }

        /// <summary>
        /// The position the event began (where mouse down was seen, or touch began)
        /// </summary>
        public Vector2 StartPosition { get; set; }

        /// <summary>
        /// Whether the event is still happeneing (mouse/touch/key not yet released)
        /// </summary>
        public Boolean Active
        {
            get => _Active;
            set => _Active = value;
        }

        /// <summary>
        /// An id associated with the event to help identify events
        /// </summary>
        public Int32 ID { get; set; }

        /// <summary>
        /// Additional data associated with this event, varies by type
        /// </summary>
        public Object Data { get; set; }

        public TimeSpan ActiveDuration { get; internal set; }

        private Boolean _Active = true;

        public void AddOnCompleteAction(Action<InputEvent> a)
        {
            if (_OnCompleteActions == null) _OnCompleteActions = new List<Action<InputEvent>>();
            _OnCompleteActions.Add(a);
        }

        public void RemoveOnCompleteAction(Action<InputEvent> a)
        {
            _OnCompleteActions?.Remove(a);
        }

        public void ClearAllOnCompleteAction()
        {
            if (_OnCompleteActions == null) return;
            _OnCompleteActions.Clear();
            _OnCompleteActions = null;
        }

        public void Complete()
        {
            List<Action<InputEvent>> actions = _OnCompleteActions?.ToList();
            _OnCompleteActions?.Clear();
            Active = false;
            if (actions != null)
            {
                foreach (Action<InputEvent> action in actions) action(this);
                actions.Clear();
            }
            Dispose();
        }

        public override void Dispose()
        {
            if (Active) Active = false;
            base.Dispose();
        }
    }
}