﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Logic;
using Bitz.Modules.Core.Foundation.Services;

namespace Bitz.Modules.Core.Input
{
    public class InputService : Service, IInputService
    {
        private readonly List<InputEvent> _ActiveInputEvents = new List<InputEvent>();
        private readonly List<Tuple<Int32, IInputListener>> _Inputlisteners = new List<Tuple<Int32, IInputListener>>();
        private readonly List<IPressable> _Pressables = new List<IPressable>();
        private readonly List<IInputSource> _InputSources = new List<IInputSource>();
        private readonly TimeSpan _PressCooldown = TimeSpan.FromMilliseconds(300);

        private readonly ILoggerService _LoggerService;
        private readonly IGameLogicService _GameLogicService;

        public InputService()
        {
            _LoggerService = Injector.GetSingleton<ILoggerService>();
            _GameLogicService = Injector.GetSingleton<IGameLogicService>();
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {

            if (DebugMode) _LoggerService.Log(LogSeverity.DEBUG, "Updating Input Service");
            if (DebugMode) _LoggerService.Log(LogSeverity.DEBUG, $"----Processing {_InputSources.Count} Input Sources");
            foreach (IInputSource source in _InputSources)
            {
                source.ProcessInputEvents(_ActiveInputEvents);
            }

            if (!(_ActiveInputEvents.Count == 0 || _Inputlisteners.Count == 0 && _Pressables.Count == 0))
            {
                List<InputEvent> currentTouchEvents = _ActiveInputEvents.ToList();
                List<Tuple<Int32, IInputListener>> inputlisteners = _Inputlisteners.OrderByDescending(a => a.Item1).ToList();

                if (DebugMode) _LoggerService.Log(LogSeverity.DEBUG, $"----Prcoessing {inputlisteners.Count} input listeners");
                foreach (Tuple<Int32, IInputListener> listener in inputlisteners)
                {
                    if (currentTouchEvents.Count == 0) break;
                    listener.Item2.ProcessInputEvents(currentTouchEvents);
                }

                List<IPressable> pressables = _Pressables.OrderByDescending(a => a.PressablePriortiy).ToList();

                if (DebugMode) _LoggerService.Log(LogSeverity.DEBUG, $"----Prcoessing {currentTouchEvents.Count} events with {pressables.Count} pressables");
                foreach (InputEvent inputEvent in currentTouchEvents.Where(a => a.Active && a.TypeOfInput != InputEvent.InputType.KEY && a.ActiveDuration == TimeSpan.Zero))
                {
                    if (DebugMode) _LoggerService.Log(LogSeverity.DEBUG, $"----/----Testing Event against {pressables.Count} pressables");
                    foreach (IPressable pressable in pressables)
                    {
                        if (DebugMode)
                        {
                            Boolean posTest = pressable.TestPressCoordinates(inputEvent.Position);
                            Boolean cooldownTest = pressable.NextCanPress <= _GameLogicService.CurrentIntervalTime;
                            _LoggerService.Log(LogSeverity.DEBUG, $"----/----Pressable Postest:{posTest} CDTest:{cooldownTest}");
                        }

                        if (pressable.TestPressCoordinates(inputEvent.Position) && pressable.NextCanPress <= _GameLogicService.CurrentIntervalTime)
                        {
                            if (DebugMode) _LoggerService.Log(LogSeverity.DEBUG, $"----/----Pressable Triggered OnPress");
                            pressable.NextCanPress = _GameLogicService.CurrentIntervalTime + _PressCooldown;
                            inputEvent.AddOnCompleteAction(a => { pressable.OnRelease?.Invoke(inputEvent); });
                            pressable.OnPress?.Invoke(inputEvent);
                            break;
                        }

                    }
                }
            }
            else
            {
                if (DebugMode) _LoggerService.Log(LogSeverity.DEBUG, $"----Skipping as no events or no listeners/pressables");
            }
            foreach (InputEvent inputEvent in _ActiveInputEvents.Where(a => a.Active))
            {
                inputEvent.ActiveDuration += timeSinceLastUpdate;
            }

        }

        public override void Initialize()
        {
            foreach (IInputSource listener in Injector.GetMultiMap<IInputSource>())
            {
                _InputSources.Add(listener);
            }
        }

        protected override void Shutdown()
        {
            foreach (IInputSource source in _InputSources.ToList())
            {
                source.Dispose();
            }
            foreach (InputEvent inputEvent in _ActiveInputEvents)
            {
                inputEvent.Dispose();
            }
            _Inputlisteners.Clear();
            _InputSources.Clear();
            _Pressables.Clear();
            _ActiveInputEvents.Clear();
        }

        public override String GetServiceDebugStatus()
        {
            return "";
        }

        public void AddInputListener(IInputListener inputListener, Int32 priority)
        {
            _Inputlisteners.Add(new Tuple<Int32, IInputListener>(priority, inputListener));
        }

        public void RemoveInputListener(IInputListener inputListener)
        {
            _Inputlisteners.RemoveAll(a => a.Item2 == inputListener);
        }

        public void AddPressable(IPressable pressable)
        {
            _Pressables.Add(pressable);
        }

        public void RemovePressable(IPressable pressable)
        {
            _Pressables.RemoveAll(a => a == pressable);
        }
    }
}