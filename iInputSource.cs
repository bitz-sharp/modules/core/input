﻿using System;
using System.Collections.Generic;
using Bitz.Modules.Core.Foundation;

namespace Bitz.Modules.Core.Input
{
    public interface IInputSource : IInjectable, IDisposable
    {
       void ProcessInputEvents(List<InputEvent> activeEvents);
    }
}